![Fresns LOGO](https://cdn.fresns.cn/images/logo-small.png)

Fresns 是一款免费开源的社交网络服务软件，专为跨平台而打造的通用型社区产品，支持灵活多样的内容形态，可以满足多种运营场景，符合时代潮流，更开放且更易于二次开发。

[点击了解产品 16 个功能特色](https://fresns.cn/guide/features.html)

## 介绍

当前 Fresns 属于「开发者测试版」，已提供框架、API、开发者文档等资料。接下来我们会用两个月时间开发一个网站引擎，提供一个网页端模板规范标准，然后面向所有人推出「正式版」，时间预计在 10 月上旬。

我们提供的「模板引擎规范」仅供开发者参考，开发者可以不遵守规范，完全根据自己的意愿开发网站前端，相当于自己独立发展前端。独立发展前端也能和其他插件兼容，并且不影响主程序同步升级，不会因为前端分叉了就脱离了主程序轨道，所以请开发者放心遵循自己的业务场景开发。

Fresns 所有功能以官网文档为主，如果遇到代码中功能与文档有差异，绝大多数的情况是代码未按文档开发，所以我们会在后续修复，极少数情况是文档写错。

## 安装教程

Fresns 是一款基于 PHP Laravel 8 框架和 MySQL 8 数据库而开发的社交网络服务软件，详情的环境要求和安装教程见官网文档。

[https://fresns.cn/guide/install.html](https://fresns.cn/guide/install.html)

## 插件示例

测试插件仓库 [https://gitee.com/fresns/tests](https://gitee.com/fresns/tests)

第三方开发的「独立部署版控制面板」和配套的「API 插件」 [https://gitee.com/jyq/fresns-control-panel](https://gitee.com/jyq/fresns-control-panel)

## 加入我们

Fresns 的开源社区正在急速增长中，如果你认可我们的开源软件，有兴趣为 Fresns 的发展做贡献，竭诚欢迎[加入我们](https://fresns.cn/community/join.html)一起开发完善。无论是[报告错误](https://fresns.cn/guide/feedback.html)或是 Pull Request 开发，那怕是修改一个错别字也是对我们莫大的帮助。

贡献指南：[https://fresns.cn/contributing/](https://fresns.cn/contributing/)

*有任何问题，欢迎联系项目发起人唐杰，参与贡献代码可加入我们开发者群*

## 联系我们

- 官方网站：[https://fresns.cn](https://fresns.cn/)
- 项目发起人：[唐杰](https://tangjie.me/)
- 电子邮箱：[jarvis.okay@gmail.com](mailto:jarvis.okay@gmail.com)
- QQ 群：[5980111](https://qm.qq.com/cgi-bin/qm/qr?k=R2pfcPUd4Nyc87AKdkuHP9yJ0MhddUaz&jump_from=webapi)
- Telegram 群：[https://t.me/fresns_zh](https://t.me/fresns_zh)
- 微信群：<img src="https://tangjie.me/media/wechat/fresns.jpg" alt="Fresns WeChat" width="300" style="vertical-align:text-top;">