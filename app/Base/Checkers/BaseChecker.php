<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

namespace App\Base\Checkers;

// 业务检查, 比如金额，状态等
// 都用静态方法即可
use App\Traits\CodeTrait;

class BaseChecker
{
    use CodeTrait;
}