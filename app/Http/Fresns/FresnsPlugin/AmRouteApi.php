<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet插件 plugin
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Http\Fresns\FresnsPlugin'], function(){
//     Route::get('/fresnsplugin/index', 'AmControllerAdmin@index')->name('admin.fresnsplugin.index');
//     Route::post('/fresnsplugin/store', 'AmControllerAdmin@store')->name('admin.fresnsplugin.store');
//     Route::post('/fresnsplugin/update', 'AmControllerAdmin@update')->name('admin.fresnsplugin.update');
//     Route::post('/fresnsplugin/destroy', 'AmControllerAdmin@destroy')->name('admin.fresnsplugin.destroy');
//     Route::get('/fresnsplugin/detail', 'AmControllerAdmin@detail')->name('admin.fresnsplugin.detail');
//     Route::get('/fresnsplugin/export', 'AmControllerAdmin@export')->name('admin.fresnsplugin.export');
// });
