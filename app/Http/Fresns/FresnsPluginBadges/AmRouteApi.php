<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// // Tweet_plugin_badges plugin_badges
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetPluginBadges'], function(){
//     Route::get('/tweetpluginbadges/index', 'AmControllerAdmin@index')->name('admin.tweetpluginbadges.index');
//     Route::post('/tweetpluginbadges/store', 'AmControllerAdmin@store')->name('admin.tweetpluginbadges.store');
//     Route::post('/tweetpluginbadges/update', 'AmControllerAdmin@update')->name('admin.tweetpluginbadges.update');
//     Route::post('/tweetpluginbadges/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetpluginbadges.destroy');
//     Route::get('/tweetpluginbadges/detail', 'AmControllerAdmin@detail')->name('admin.tweetpluginbadges.detail');
//     Route::get('/tweetpluginbadges/export', 'AmControllerAdmin@export')->name('admin.tweetpluginbadges.export');
// });
