<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet_file附表 file_appends
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetFileAppends'], function(){
//     Route::get('/tweetfileappends/index', 'AmControllerAdmin@index')->name('admin.tweetfileappends.index');
//     Route::post('/tweetfileappends/store', 'AmControllerAdmin@store')->name('admin.tweetfileappends.store');
//     Route::post('/tweetfileappends/update', 'AmControllerAdmin@update')->name('admin.tweetfileappends.update');
//     Route::post('/tweetfileappends/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetfileappends.destroy');
//     Route::get('/tweetfileappends/detail', 'AmControllerAdmin@detail')->name('admin.tweetfileappends.detail');
//     Route::get('/tweetfileappends/export', 'AmControllerAdmin@export')->name('admin.tweetfileappends.export');
// });
