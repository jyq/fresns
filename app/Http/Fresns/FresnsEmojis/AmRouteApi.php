<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet表情包 emojis
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetEmojis'], function(){
//     Route::get('/tweetemojis/index', 'AmControllerAdmin@index')->name('admin.tweetemojis.index');
//     Route::post('/tweetemojis/store', 'AmControllerAdmin@store')->name('admin.tweetemojis.store');
//     Route::post('/tweetemojis/update', 'AmControllerAdmin@update')->name('admin.tweetemojis.update');
//     Route::post('/tweetemojis/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetemojis.destroy');
//     Route::get('/tweetemojis/detail', 'AmControllerAdmin@detail')->name('admin.tweetemojis.detail');
//     Route::get('/tweetemojis/export', 'AmControllerAdmin@export')->name('admin.tweetemojis.export');
// });
