<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet会员 members
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Http\Fresns\FresnsMembers'], function(){
//     Route::get('/fresnsmembers/index', 'AmControllerAdmin@index')->name('admin.fresnsmembers.index');
//     Route::post('/fresnsmembers/store', 'AmControllerAdmin@store')->name('admin.fresnsmembers.store');
//     Route::post('/fresnsmembers/update', 'AmControllerAdmin@update')->name('admin.fresnsmembers.update');
//     Route::post('/fresnsmembers/destroy', 'AmControllerAdmin@destroy')->name('admin.fresnsmembers.destroy');
//     Route::get('/fresnsmembers/detail', 'AmControllerAdmin@detail')->name('admin.fresnsmembers.detail');
//     Route::get('/fresnsmembers/export', 'AmControllerAdmin@export')->name('admin.fresnsmembers.export');

// });
