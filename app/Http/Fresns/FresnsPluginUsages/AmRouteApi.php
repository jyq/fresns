<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet_plugin_sence plugin_scene
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetPluginUsages'], function(){
//     Route::get('/tweetpluginscene/index', 'AmControllerAdmin@index')->name('admin.tweetpluginscene.index');
//     Route::post('/tweetpluginscene/store', 'AmControllerAdmin@store')->name('admin.tweetpluginscene.store');
//     Route::post('/tweetpluginscene/update', 'AmControllerAdmin@update')->name('admin.tweetpluginscene.update');
//     Route::post('/tweetpluginscene/updateRankNum', 'AmControllerAdmin@updateRankNum')->name('admin.tweetpluginscene.updateRankNum');
//     Route::post('/tweetpluginscene/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetpluginscene.destroy');
//     Route::get('/tweetpluginscene/detail', 'AmControllerAdmin@detail')->name('admin.tweetpluginscene.detail');
//     Route::get('/tweetpluginscene/export', 'AmControllerAdmin@export')->name('admin.tweetpluginscene.export');
//     Route::get('/tweetpluginscene/addLangCode', 'AmControllerAdmin@addLangCode')->name('admin.tweetpluginscene.addLangCode');
// });
