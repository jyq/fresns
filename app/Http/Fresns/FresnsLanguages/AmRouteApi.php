<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// // Tweet语言 languages
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetLanguages'], function(){
//     Route::get('/tweetlanguages/index', 'AmControllerAdmin@index')->name('admin.tweetlanguages.index');
//     Route::post('/tweetlanguages/store', 'AmControllerAdmin@store')->name('admin.tweetlanguages.store');
//     //新增多语言
//     Route::post('/tweetlanguages/configStore', 'AmControllerAdmin@configStore')->name('admin.tweetlanguages.configStore');
//     Route::post('/tweetlanguages/update', 'AmControllerAdmin@update')->name('admin.tweetlanguages.update');
//     Route::post('/tweetlanguages/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetlanguages.destroy');
//     Route::get('/tweetlanguages/detail', 'AmControllerAdmin@detail')->name('admin.tweetlanguages.detail');
//     Route::get('/tweetlanguages/export', 'AmControllerAdmin@export')->name('admin.tweetlanguages.export');
// });
