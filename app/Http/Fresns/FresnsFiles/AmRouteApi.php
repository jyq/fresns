<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet_files files
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetFiles'], function(){
//     Route::get('/tweetfiles/index', 'AmControllerAdmin@index')->name('admin.tweetfiles.index');
//     Route::post('/tweetfiles/store', 'AmControllerAdmin@store')->name('admin.tweetfiles.store');
//     Route::post('/tweetfiles/update', 'AmControllerAdmin@update')->name('admin.tweetfiles.update');
//     Route::post('/tweetfiles/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetfiles.destroy');
//     Route::get('/tweetfiles/detail', 'AmControllerAdmin@detail')->name('admin.tweetfiles.detail');
//     Route::get('/tweetfiles/export', 'AmControllerAdmin@export')->name('admin.tweetfiles.export');
//     Route::post('/tweetfiles/upload', 'AmControllerAdmin@upload')->name('admin.tweetfiles.upload');
// });
