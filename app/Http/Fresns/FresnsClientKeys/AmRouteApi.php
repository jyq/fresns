<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet_client_keys client_keys
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Http\Fresns\FresnsClientKeys'], function(){
//     Route::get('/tweetclientkeys/index', 'AmControllerAdmin@index')->name('admin.tweetclientkeys.index');
//     Route::post('/tweetclientkeys/store', 'AmControllerAdmin@store')->name('admin.tweetclientkeys.store');
//     Route::post('/tweetclientkeys/update', 'AmControllerAdmin@update')->name('admin.tweetclientkeys.update');
//     Route::post('/tweetclientkeys/referSecret', 'AmControllerAdmin@referSecret')->name('admin.tweetclientkeys.referSecret');
//     Route::post('/tweetclientkeys/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetclientkeys.destroy');
//     Route::get('/tweetclientkeys/detail', 'AmControllerAdmin@detail')->name('admin.tweetclientkeys.detail');
//     Route::get('/tweetclientkeys/export', 'AmControllerAdmin@export')->name('admin.tweetclientkeys.export');
// });
