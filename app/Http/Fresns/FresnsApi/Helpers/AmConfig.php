<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

namespace App\Http\Fresns\FresnsApi\Helpers;

class AmConfig
{
    const IMG_SETTING = 'img_setting';
    const VIDEO_SETTING = 'video_setting';
    const AUDIO_SETTING = 'audio_setting';
    const FILE_SETTING = 'file_setting';
    const BACKEND_DOMAIN = 'backend_domain';
}