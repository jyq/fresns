<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

namespace App\Http\Fresns\FresnsApi\Base;


use App\Base\Services\BaseAdminService;

class FresnsBaseService extends BaseAdminService
{
    public $needCommon = false;

}
