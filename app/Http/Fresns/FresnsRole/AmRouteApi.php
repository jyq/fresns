<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet_role role
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Plugins\Tweet\TweetRole'], function(){
//     Route::get('/tweetrole/index', 'AmControllerAdmin@index')->name('admin.tweetrole.index');
//     Route::post('/tweetrole/store', 'AmControllerAdmin@store')->name('admin.tweetrole.store');
//     Route::post('/tweetrole/update', 'AmControllerAdmin@update')->name('admin.tweetrole.update');
//     Route::post('/tweetrole/destroy', 'AmControllerAdmin@destroy')->name('admin.tweetrole.destroy');
//     Route::get('/tweetrole/detail', 'AmControllerAdmin@detail')->name('admin.tweetrole.detail');
//     Route::get('/tweetrole/export', 'AmControllerAdmin@export')->name('admin.tweetrole.export');
// });
