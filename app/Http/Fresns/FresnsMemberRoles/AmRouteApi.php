<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

// Tweet_member_roles member_roles
// Route::group(['prefix' => 'admin', 'middleware' => 'auth:api', 'namespace' => '\App\Http\Fresns\FresnsMemberRoles'], function(){
//     Route::get('/fresnsmemberroles/index', 'AmControllerAdmin@index')->name('admin.fresnsmemberroles.index');
//     Route::post('/fresnsmemberroles/store', 'AmControllerAdmin@store')->name('admin.fresnsmemberroles.store');
//     Route::post('/fresnsmemberroles/update', 'AmControllerAdmin@update')->name('admin.fresnsmemberroles.update');
//     Route::post('/fresnsmemberroles/destroy', 'AmControllerAdmin@destroy')->name('admin.fresnsmemberroles.destroy');
//     Route::get('/fresnsmemberroles/detail', 'AmControllerAdmin@detail')->name('admin.fresnsmemberroles.detail');
//     Route::get('/fresnsmemberroles/export', 'AmControllerAdmin@export')->name('admin.fresnsmemberroles.export');

//     //合并
//     Route::post('/fresnsmemberroles/merge', 'AmControllerAdmin@merge')->name('admin.fresnsmember.merge');
// });
