<?php

/*
 * Fresns
 * Copyright (C) 2021-Present 唐杰
 * Released under the Apache-2.0 License.
 */

namespace App\Http\Center\Base;

use App\Base\Controllers\BaseController;

/**
 * 插件场景基础类
 * 处理插件的安装/卸载/升级等基础操作
 */
class BaseSceneController extends BaseController
{

}
